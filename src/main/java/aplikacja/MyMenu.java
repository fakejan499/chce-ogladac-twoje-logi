package aplikacja;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa z menu.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyMenu extends JMenuBar {
    private final JDialog oAutorzeDialog = new OAutorze();
    private final JDialog pomoc = new Pomoc();

    /**
     * Domyślny konstruktor. Tworzy całe menu.
     * @param frame główny frame aplikacji
     */
    public MyMenu(MyFrame frame) {
        super();
        JMenu menu = new JMenu("Akcje");
        JMenuItem menuItemReset = new JMenuItem("Resetuj");
        menuItemReset.setIcon(Icons.resizeImage(Icons.RESET, 16));
        menuItemReset.addActionListener(e -> frame.resetTable());
        JMenuItem menuItemAdd = new JMenuItem("Dodaj");
        menuItemAdd.setIcon(Icons.resizeImage(Icons.PLUS, 16));
        menuItemAdd.addActionListener(e -> frame.addValueToTable());
        JMenuItem menuItemSave = new JMenuItem("Zapisz");
        menuItemSave.setIcon(Icons.resizeImage(Icons.SAVE, 16));
        menuItemSave.addActionListener(e -> frame.saveValuesToFile());
        menu.add(menuItemReset);
        menu.add(menuItemAdd);
        menu.add(menuItemSave);
        this.add(menu);
        JMenuItem oAut = new JMenuItem("O Autorze");
        oAut.setIcon(Icons.resizeImage(Icons.USER, 16));
        oAut.setMaximumSize(new Dimension(100, 100));
        oAut.addActionListener((e) -> {
            oAutorzeDialog.setVisible(true);
            frame.otworzonoInfoOAutorze();
        });
        this.add(oAut);
        JMenuItem help = new JMenuItem("Pomoc");
        help.setIcon(Icons.resizeImage(Icons.HELP, 16));
        help.setMaximumSize(new Dimension(80, 100));
        help.addActionListener(e -> {
            pomoc.setVisible(true);
            frame.otworzonoPomoc();
        });
        this.add(help);
    }
}
