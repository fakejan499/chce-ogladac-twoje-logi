package aplikacja;

import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa okna dialogowego z pomocą do aplikacji.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class Pomoc extends JDialog {
    /**
     * Domyślny konstruktor. Tworzy i wyświetla okno dialogowe z pomocą do programu.
     * Dzieli się na dwie główne części: wprowadzenie oraz charakterystyka i funkcje.
     */
    public Pomoc() {
        setSize(new Dimension(500, 700));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        setTitle("Pomoc");
        setLayout(new MigLayout("wrap 2"));

        JPanel wprowadzenie = new JPanel(new MigLayout());
        JLabel wprowadzenieLabel = new JLabel("WPROWADZENIE");
        wprowadzenieLabel.setForeground(new Color(255, 0, 0));
        wprowadzenie.add(wprowadzenieLabel);
        wprowadzenie.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(255, 0, 0)));
        JLabel wprowadzenieText = new JLabel(
                "<html><body>Witam cię w cudownej aplikacji na jeszcze lepszy przedmiot.<br><br>" +
                        "Używanie jej nie należy do najtrudniejszych czynności jake w życiu wykonywałeś " +
                        "(przynajmniej mam taką nadzieję).</body></html>"
        );
        wprowadzenieText.setBorder(BorderFactory.createMatteBorder(0, 0, 50, 0, new Color(255, 0, 0, 1)));
        add(wprowadzenie, new CC().pushX().grow().spanX());
        add(wprowadzenieText, new CC().spanX());


        JPanel charakterystyka = new JPanel(new MigLayout());
        JLabel charakterystykaLabel = new JLabel("<html><body>CHARAKTERYSTYKA I FUNKCJE</body></html>");
        charakterystykaLabel.setForeground(new Color(255, 0, 0));
        charakterystyka.add(charakterystykaLabel);
        charakterystyka.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(255, 0, 0)));
        JLabel charakterystykaText = new JLabel(
                "<html><body>" +
                        "Ta niesamowita aplikacja pozwala na zrobienie rzeczy, o których ci się nie śniło.<br><br>" +
                        "Możesz wprowadzić liczbę do tabeli widocznej na środku ekranu programu. Aby to zrobić " +
                        "musisz prowadzić liczbę w miejscu gdzie widnieje napis \"Wprowadz liczbe\". " +
                        "Następnie wybierasz numer wiersza i kolumny (to te suwaki, tak tylko piszę gdybyś drogi" +
                        " użytkowniku nie potrafił czytać). Na koniec klikasz przycisk \"Dodaj\" (część główna, " +
                        "tool bar po prawej stronie, lub w akcjach na górze okna) i wartość się dodaje.<br><br>" +
                        "Wartości tabeli można również resetować (ustawiać na 0 w każdej komórce) oraz zapisywać " +
                        "do pliku. Służą do tego odpowiednio przyciski Resetuj oraz Zapisz, znajdują się one w " +
                        "części głównej, tool barze po prawej stronie i w akcjach na górze okna.<br><br>" +
                        "Operacja oznacza operację wykonywaną na liczbach w komórkach tabeli.<br>" +
                        "Ta liczba nad operacją to wynik operacji.<br><br>" +
                        "Inne opcje jakie można znaleźć w menu (musisz kliknąć) to informacje o autorze " +
                        "oraz pomoc (no jestem w szoku, że dotarłeś do pomocy bez czytania pomycy jak dotrzeć " +
                        "do pomocy).<br><br>" +
                        "</body></html>"
        );
        add(charakterystyka, new CC().pushX().grow().spanX());
        add(charakterystykaText, new CC().spanX());
    }
}