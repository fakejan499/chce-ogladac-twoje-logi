package aplikacja;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * Super klasa z ikomani.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public final class Icons {
    /**
     * Ikona help.
     */
    public final static ImageIcon HELP = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("help.png")));
    /**
     * Ikona plusa.
     */
    public final static ImageIcon PLUS = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("plus.png")));
    /**
     * Ikona resetowania.
     */
    public final static ImageIcon RESET = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("reset.png")));
    /**
     * Ikona zapisywania.
     */
    public final static ImageIcon SAVE = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("save.png")));
    /**
     * Ikona user (z jakąś postacią).
     */
    public final static ImageIcon USER = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("user.png")));
    /**
     * Ikona domu.
     */
    public final static ImageIcon HOME = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("home.png")));
    /**
     * Ikona kalendarza.
     */
    public final static ImageIcon CALENDAR = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("calendar.png")));
    /**
     * Ikona wykresu.
     */
    public final static ImageIcon CHART = new ImageIcon(Objects.requireNonNull(Icons.class.getResource("chart.png")));


    /**
     * Statyczna metoda pozwalająca na zmianę wielkości ikony.
     *
     * @param icon ikona której rozmiar chcemy zmienić
     * @param size rozmiar w px
     * @return ikona ze zmienionym rozmiarem
     */
    public static ImageIcon resizeImage(ImageIcon icon, int size) {
        BufferedImage resizedImg = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(icon.getImage(), 0, 0, size, size, null);
        g2.dispose();

        return new ImageIcon(resizedImg);
    }
}
