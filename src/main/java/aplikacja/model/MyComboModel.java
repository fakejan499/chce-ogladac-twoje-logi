package aplikacja.model;

import aplikacja.processor.AvgProcessor;
import aplikacja.processor.MinMaxProcessor;
import aplikacja.processor.Processor;
import aplikacja.processor.SumProcessor;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.Arrays;
import java.util.List;

/**
 * Mój model dla komponentu combo.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyComboModel implements ComboBoxModel {
    private Processor selectedItem;
    private final List<Processor> itemsList;

    /**
     * Tworzy obiekt MyComboModel z procesorami.
     */
    public MyComboModel() {
        Processor[] processors = {SumProcessor.getInstance(), AvgProcessor.getInstance(), MinMaxProcessor.getInstance()};
        itemsList = Arrays.asList(processors);
        selectedItem = itemsList.get(0);
    }

    /**
     * Ustawia wybrany element.
     *
     * @param anItem nowy wybrany item
     */
    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (Processor) anItem;
    }

    /**
     * Pobiera wybrany element.
     *
     * @return wybrany element
     */
    @Override
    public Processor getSelectedItem() {
        return selectedItem;
    }

    /**
     * Pobiera ilość elementów w modelu.
     *
     * @return ilość elementów w modelu
     */
    @Override
    public int getSize() {
        return itemsList.size();
    }

    /**
     * Pobiera element o podanym indeksie.
     *
     * @param index indeks elementu
     * @return objekt znajdujący się na podanym indeksie
     */
    @Override
    public Object getElementAt(int index) {
        return itemsList.get(index);
    }

    /**
     * Nie robi nic, nadpisane z konieczności.
     */
    @Override
    public void addListDataListener(ListDataListener l) {

    }

    /**
     * Nie robi nic, nadpisane z konieczności.
     */
    @Override
    public void removeListDataListener(ListDataListener l) {

    }
}
