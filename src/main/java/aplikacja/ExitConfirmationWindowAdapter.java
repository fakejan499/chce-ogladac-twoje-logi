package aplikacja;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Rozszerzony WindowAdapter, z zabezpieczonym zamykaniem aplikacji.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class ExitConfirmationWindowAdapter extends WindowAdapter {
    /**
     * Domyślny konstruktor.
     */
    public ExitConfirmationWindowAdapter() {
    }

    /**
     * Zabezpiecza zamykanie aplikacji poprzez zadanie pytania czy user chce zamknąć aplikację.
     * @param e zdarzenie okna
     */
    @Override
    public void windowClosing(WindowEvent e) {
        JFrame frame = (JFrame)e.getSource();
        int confirm = JOptionPane.showOptionDialog(
                null, "Czy na pewno chcesz wyjść z aplikacji?",
                "Potwierdzenie zamknięcia", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);
        if (confirm == JOptionPane.YES_OPTION)
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
