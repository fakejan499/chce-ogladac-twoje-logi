package aplikacja;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Panel wprowadzania liczby.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class PanelWprowadzaniaLiczby extends JPanel {
    private JTextField dataInpTextField;

    /**
     * Domyślny konstruktor tworzący panel z text fieldem.
     * @param frame główny frame aplikacji
     */
    PanelWprowadzaniaLiczby(MyFrame frame) {
        super();

        JLabel tfLabel = new JLabel("Wprowadz liczbe:");
        add(tfLabel);

        dataInpTextField = new JTextField("0", 20);
        dataInpTextField.setHorizontalAlignment(JTextField.CENTER);
        dataInpTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                frame.wprowadzonoText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                frame.usunietoText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        });
        add(dataInpTextField);
    }

    /**
     * Pobiera wartość zapisaną w polu tekstowym.
     * @return wartość w postaci ciągu znaków
     */
    public String getValue() {
        return dataInpTextField.getText();
    }
}
