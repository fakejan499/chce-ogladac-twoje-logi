package aplikacja.processor;

import javax.swing.table.TableModel;

/**
 * Procesor wyliczający średnią arytmetyczną.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class AvgProcessor implements Processor {
    private static AvgProcessor instance = null;

    private AvgProcessor(){}

    /**
     * Zwraca procesor AvgProcessor.
     * @return instancja AvgProcessora
     */
    public static AvgProcessor getInstance() {
        if (instance == null) instance = new AvgProcessor();
        return instance;
    }

    /**
     * Wylicza średnią arytmetyczą z liczb ze wszsytkich komórek modelu tabeli.
     * @param data model tabeli, na której komórkach wykonywana będzie operacja
     * @return średnia arytmetyczna
     */
    @Override
    public String process(TableModel data) {
        double sum = 0;
        int el = 0;
        for (int i = 0; i < data.getRowCount(); i++) {
            for (int j = 0; j < data.getColumnCount(); j++) {
                sum += (Double) data.getValueAt(i, j);
                el++;
            }
        }
        return String.valueOf(sum / el);
    }

    /**
     * Zwraca nazwe jako String.
     * @return "Średnia"
     */
    @Override
    public String toString() {
        return "Średnia";
    }
}
