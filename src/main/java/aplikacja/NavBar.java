package aplikacja;

import com.l2fprod.common.swing.JButtonBar;
import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa panelu nawigacyjnego aplikacji.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class NavBar extends JPanel {
    private final MyFrame frame;

    /**
     * Domyśly konstruktor. Tworzy panel nawigacji z kontrolkami nawygacyjnymi do strony głównej,
     * kalendarza i wykresu.
     * @param frame główny frame aplikajci
     */
    NavBar(MyFrame frame) {
        super();
        this.frame = frame;
        setLayout(new MigLayout("wrap 1"));
        setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, new Color(100, 100, 100)));

        JButtonBar buttonBar = new JButtonBar();
        buttonBar.setOrientation(JButtonBar.VERTICAL);

        JButton home = new JButton("<html><body><center>Strona Główna</center></body></html>");
        home.setIcon(Icons.HOME);
        JButton calendar = new JButton("<html><body><center>Calendar</center></body></html>");
        calendar.setIcon(Icons.CALENDAR);
        JButton chart = new JButton("<html><body><center>Wykres</center></body></html>");
        chart.setIcon(Icons.CHART);
        buttonBar.add(home);
        buttonBar.add(calendar);
        buttonBar.add(chart);
        home.addActionListener(e -> frame.navigateToHome());
        calendar.addActionListener(e -> frame.navigateToCalendar());
        chart.addActionListener(e -> frame.navigateToChart());
        add(buttonBar, new CC().grow().push());
    }
}
