package aplikacja;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa okna dialogowego z informacjami o autorze.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class OAutorze extends JDialog {
    /**
     * Domyślny konstruktor. Tworzy okno dialogowe z informacjami o atuorze.
     */
    public OAutorze() {
        setSize(new Dimension(350, 200));
        setLocationRelativeTo(null);
        setTitle("O Autorze");
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        setLayout(new MigLayout("wrap 2"));
        add(new Label("Imię: "));
        add(new Label("Dawid"));
        add(new Label("Nazwisko: "));
        add(new Label("Szemro"));
        add(new Label("Kierunek: "));
        add(new Label("Informatyka"));
        add(new Label("Aplikacja: "));
        add(new Label("Projektowanie Aplikacji"));
        add(new Label("Licencja: "));
        add(new Label("MIT"));
    }
}
