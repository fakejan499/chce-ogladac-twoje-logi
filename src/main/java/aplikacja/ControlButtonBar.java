package aplikacja;

import javax.swing.*;

/**
 * Panel z kontrolkami.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class ControlButtonBar extends JPanel {
    private final MyFrame frame;

    /**
     * Tworzy nowy panel z buttonami do resetowania, dodawania i zapisywania.
     * @param frame główny frame aplikacji
     */
    ControlButtonBar(MyFrame frame) {
        super();
        this.frame = frame;

        JButton resetButton = new JButton("Resetuj");
        resetButton.setIcon(Icons.resizeImage(Icons.RESET, 20));
        resetButton.addActionListener(e -> frame.resetTable());
        add(resetButton);

        JButton addButton = new JButton("Dodaj");
        addButton.setIcon(Icons.resizeImage(Icons.PLUS, 20));
        addButton.addActionListener((event) -> frame.addValueToTable());
        add(addButton);
        JButton saveButton = new JButton("Zapisz");
        saveButton.setIcon(Icons.resizeImage(Icons.SAVE, 20));
        saveButton.addActionListener((event) -> frame.saveValuesToFile());
        add(saveButton);
    }
}
