package aplikacja;

import javax.swing.*;

/**
 * Prosty panel z polem tekstowym.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class PanelWyniku extends JPanel {
    private final JTextArea textArea;
    private final MyFrame frame;

    /**
     * Domyślny konstruktor. Tworzy panel z nieedytowalnym polem tekstowym.
     * @param frame główny frame aplikajci
     */
    public PanelWyniku(MyFrame frame) {
        super();
        this.frame = frame;

        textArea = new JTextArea();
        textArea.setEditable(false);
        add(textArea);
    }

    /**
     * Ustawianie tekstu pola tekstowego.
     * @param text nowy tekst pola tekstowego
     */
    public void setText(String text) {
        textArea.setText(text);
    }
}
