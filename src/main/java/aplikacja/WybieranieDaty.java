package aplikacja;

import org.freixas.jcalendar.JCalendarCombo;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Klasa panelu z elementem combo do wybierania daty.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class WybieranieDaty extends JPanel {
    /**
     * Format wyświetlania daty.
     */
    public static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private MyFrame frame;

    /**
     * Domyślny konstruktor. Tworzy panel z komponentem combo JCalendarCombo.
     * @param frame główny frame aplikacji
     */
    WybieranieDaty(MyFrame frame) {
        super();
        this.frame = frame;

        JCalendarCombo calendarCombo = new JCalendarCombo();
        calendarCombo.setDateFormat(FORMAT);

        calendarCombo.addDateListener(dateEvent -> frame.wybranoDate(dateEvent.getSelectedDate(), false));
        frame.wybranoDate(calendarCombo.getCalendar(), true);

        add(calendarCombo);
    }
}
