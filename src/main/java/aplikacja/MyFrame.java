package aplikacja;

import aplikacja.model.MyComboModel;
import aplikacja.model.MyTableModel;
import aplikacja.processor.Processor;
import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

/**
 * Klasa głównego frejma, pełniąca również funkcję mediatora między komponentami.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyFrame extends JFrame {
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 700;

    private final JOptionPane optionPane = new JOptionPane();

    private JPanel center;
    private final JPanel glownaCzescCentralna, kalendarzCzescCentralna;

    private final PanelWprowadzaniaLiczby panelWprowadzaniaLiczby;
    private final PasekStatusu pasekStatusu;
    private final MyComboModel selectModel;
    private final MyTableModel tableModel;
    private final PanelWyboruKomorki panelWyboruKomorki;
    private final MyMenu menuBar;
    private final MyToolBar toolBar;
    private final ControlButtonBar panelKontorli;
    private final PanelWyniku glownyPanelWyniku;

    private final WybieranieDaty wybieranieDaty;
    private final PanelWyniku panelWynikuDaty;

    /**
     * Domyślny konstruktor, składa całą aplikację (wszystko komponenty) do kupy.
     */
    MyFrame() {
        setTitle("Projektowanie Aplikacji");
        setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new ExitConfirmationWindowAdapter());
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setLayout(new BorderLayout());

        menuBar = new MyMenu(this);
        toolBar = new MyToolBar(this);
        pasekStatusu = new PasekStatusu();

        center = glownaCzescCentralna = new JPanel(new MigLayout("wrap 1", "center"));

        panelWprowadzaniaLiczby = new PanelWprowadzaniaLiczby(this);
        panelWyboruKomorki = new PanelWyboruKomorki(this);
        tableModel = new MyTableModel(5, 5);
        panelKontorli = new ControlButtonBar(this);
        glownyPanelWyniku = new PanelWyniku(this);
        selectModel = new MyComboModel();

        glownaCzescCentralna.add(panelWprowadzaniaLiczby, new CC().push());
        glownaCzescCentralna.add(panelWyboruKomorki, new CC().push());
        glownaCzescCentralna.add(new MyTable(tableModel), new CC().push().grow());
        glownaCzescCentralna.add(panelKontorli, new CC().push());
        glownaCzescCentralna.add(glownyPanelWyniku, new CC().push());
        glownaCzescCentralna.add(new PoleWyboru(selectModel, this), new CC().push());


        kalendarzCzescCentralna = new JPanel(new MigLayout("wrap 2", "center"));

        JLabel labelDaty = new JLabel("Wybrana data: ");
        panelWynikuDaty = new PanelWyniku(this);
        wybieranieDaty = new WybieranieDaty(this);

        kalendarzCzescCentralna.add(labelDaty, new CC().pushX().alignX("right"));
        kalendarzCzescCentralna.add(panelWynikuDaty, new CC().pushX().alignX("left"));
        kalendarzCzescCentralna.add(wybieranieDaty, new CC().spanX().pushX());


        contentPane.add(menuBar, BorderLayout.NORTH);
        contentPane.add(center, BorderLayout.CENTER);
        contentPane.add(toolBar, BorderLayout.EAST);
        contentPane.add(pasekStatusu, BorderLayout.SOUTH);

        contentPane.add(new NavBar(this), BorderLayout.WEST);

        processData();

        setVisible(true);
        new MyTip();
    }

    /**
     * Metoda, która powinna być wykonywana, jeśli otworzono info o autorze.
     */
    public void otworzonoInfoOAutorze() {
        pasekStatusu.setStatus("Otworzono informacje o autorze");
    }

    /**
     * Metoda, która powinna być wykonywana, jeśli otworzono pomoc.
     */
    public void otworzonoPomoc() {
        pasekStatusu.setStatus("Otworzono pomoc");
    }

    /**
     * Metoda, która powinna byc wywoływana jeśli zminiono wartość w combo z wyborem procesowania tablicy.
     * Procesuje dane z tablicy i wyświetla je.
     */
    public void onComboValChanged() {
        processData();
        pasekStatusu.setStatus("Zmieniono operację");
    }

    private void processData() {
        Processor processor = selectModel.getSelectedItem();
        String res = processor.process(tableModel);
        glownyPanelWyniku.setText(res);
    }

    /**
     * Przywraca wszystkie wartości (wsyzstkie komórki) w tabeli na 0.
     * Procesuje dane i wyświetla status.
     */
    public void resetTable() {
        tableModel.resetTable();
        processData();
        pasekStatusu.setStatus("Zresetowano tabelę");
    }

    /**
     * Dodaje wartość do tablicy. Wartość i komórka są wyliczne na podstawie wartości z kontrolek w komponentach.
     */
    public void addValueToTable() {
        try {
            int row = panelWyboruKomorki.getRowSlider().getValue() - 1;
            int col = panelWyboruKomorki.getColSlider().getValue() - 1;
            double val = Double.parseDouble(panelWprowadzaniaLiczby.getValue());
            tableModel.setValueAt(val, row, col);
            processData();
            pasekStatusu.setStatus("Dodano wartość do tabeli");
        } catch (IllegalArgumentException e) {
            optionPane.setMessage("Cos pomieszles z liczba koleżko.");
            optionPane.createDialog("Ty baranie").setVisible(true);
            return;
        }
    }

    /**
     * Zapisuje wartości z tablicy do wskazanego przez użytkownika pliku.
     */
    public void saveValuesToFile() {
        JFileChooser fileChooser = new JFileChooser();
        int option = fileChooser.showSaveDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            writeData(file);
            pasekStatusu.setStatus("Zapisano tabelę do pliku");
        } else {
        }
    }

    private void writeData(File file) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write("");
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                for (int j = 0; j < tableModel.getColumnCount(); j++) {
                    fileWriter.append(tableModel.getValueAt(i, j).toString()).append("\t");
                }
                fileWriter.append("\n");
            }
        } catch (IOException ioException) {
            optionPane.setMessage("Zapis do pliku nie wyszedl!");
            optionPane.createDialog("Cos sie popsulo").setVisible(true);
        }
    }

    /**
     * Metoda która powynna być wywoływana, kiedy wprowadzono tekst do dowolnego pola tekstowego w aplikacji.
     * Wyświetla status w pasku statusu.
     */
    public void wprowadzonoText() {
        pasekStatusu.setStatus("Wprowadzono text");
    }

    /**
     * Metoda która powynna być wywoływana, kiedy usunięto tekst do dowolnego pola tekstowego w aplikacji.
     * Wyświetla informację w pasku statusu.
     */
    public void usunietoText() {
        pasekStatusu.setStatus("Usunięto text");
    }

    /**
     * Nawiguje w aplikacji do części domyślnej (startowej).
     */
    public void navigateToHome() {
        getContentPane().remove(center);
        getContentPane().add(center = glownaCzescCentralna, BorderLayout.CENTER);
        toolBar.setVisible(true);
        repaint();
        pasekStatusu.setStatus("Nawigowano do czesci glownej");
    }

    /**
     * Nawiguje w aplikacji do częśli z wyborem daty.
     */
    public void navigateToCalendar() {
        getContentPane().remove(center);
        getContentPane().add(center = kalendarzCzescCentralna, BorderLayout.CENTER);
        toolBar.setVisible(false);
        repaint();
        pasekStatusu.setStatus("Nawigowano do kalendarza");
    }

    /**
     * Nawiguje w nawigacji do części z wykresem.
     */
    public void navigateToChart() {
        getContentPane().remove(center);
        toolBar.setVisible(false);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            for (int j = 0; j < tableModel.getColumnCount(); j++) {
                dataset.addValue((Double) tableModel.getValueAt(i, j), new Integer(i + 1), new Integer(j + 1));
            }
        }
        JFreeChart chart = ChartFactory.createBarChart(
                "Mój Wykres",
                "Komórka",
                "Wartość",
                dataset,
                PlotOrientation.HORIZONTAL,
                true,
                true,
                false
        );
        ChartPanel panel = new ChartPanel(chart);
        getContentPane().add(center = panel, BorderLayout.CENTER);
        repaint();
        pasekStatusu.setStatus("Nawigowano do Wkresu");
    }

    /**
     * Wyświetla wybraną datę w polu tekstowym oraz wyświetla informację o tym w pasku statusu.
     *
     * @param selectedDate wybrana data
     */
    public void wybranoDate(Calendar selectedDate, boolean init) {
        panelWynikuDaty.setText(WybieranieDaty.FORMAT.format(selectedDate.getTime()));
        if (!init) pasekStatusu.setStatus("Wybrano datę");
    }
}
