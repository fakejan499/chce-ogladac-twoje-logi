package aplikacja;

import javax.swing.*;
import java.awt.*;

/**
 * Panel z kontrolkami do wyboru komórki.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class PanelWyboruKomorki extends JPanel {
    private final JSlider rowSlider;
    private final JSlider colSlider;

    private final MyFrame frame;

    /**
     * Domyślny konstruktor. Tworzy panel z dwoma sliderami do wyboru komórki i wiersza tabeli.
     *
     * @param frame główny frame aplikacji
     */
    PanelWyboruKomorki(MyFrame frame) {
        super(new BorderLayout());
        this.frame = frame;

        JPanel jPanelR = new JPanel();
        JLabel rowLabel = new JLabel("Podaj numer wiersza:");
        rowSlider = new JSlider(1, 5);
        rowSlider.setValue(1);
        rowSlider.setPaintLabels(true);
        rowSlider.setLabelTable(rowSlider.createStandardLabels(1));
        jPanelR.add(rowLabel);
        jPanelR.add(rowSlider);
        add(jPanelR, BorderLayout.NORTH);

        JPanel jPanelC = new JPanel();
        JLabel columnLabel = new JLabel("Podaj numer kolumny:");
        colSlider = new JSlider(1, 5);
        colSlider.setValue(1);
        colSlider.setPaintLabels(true);
        colSlider.setLabelTable(rowSlider.createStandardLabels(1));
        jPanelC.add(columnLabel);
        jPanelC.add(colSlider);
        add(jPanelC, BorderLayout.SOUTH);
    }

    /**
     * Pobiera slider odpowiadający za numer wiersza
     *
     * @return slider
     */
    public JSlider getRowSlider() {
        return rowSlider;
    }

    /**
     * Pobiera slider odpowiadający za numer kolumny
     *
     * @return slider
     */
    public JSlider getColSlider() {
        return colSlider;
    }
}
