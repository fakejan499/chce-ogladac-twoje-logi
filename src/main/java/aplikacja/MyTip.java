package aplikacja;

import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.TipModel;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Klasa pola dialogowego z poradą dnia.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyTip extends JDialog {

    /**
     * Domyślny konstruktor. Wyświetla okno dialogowe z losową poradą dnia.
     */
    MyTip() {
        super();
        TipModel.Tip[] tips = {
                new DefaultTip("test", "Projektowanie aplikacji jest cudowne"),
                new DefaultTip("test", "Picie wody po pierogach może ci zaszkodzić"),
                new DefaultTip("test", "Studia nie zawsze są najlepszym wyborem"),
                new DefaultTip("test", "Ta aplikacja jest elegancka"),
        };
        TipModel tipModel = new DefaultTipModel(tips);
        JTipOfTheDay tipOfTheDay = new JTipOfTheDay(tipModel);

        tipOfTheDay.setCurrentTip(new Random().nextInt(tipModel.getTipCount()));
        add(tipOfTheDay);
        setName("Porada dnia");
        setSize(new Dimension(350, 175));
        setMinimumSize(new Dimension(350, 175));
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
