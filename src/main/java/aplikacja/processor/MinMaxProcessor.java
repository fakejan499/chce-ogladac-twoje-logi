package aplikacja.processor;

import javax.swing.table.TableModel;

/**
 * Procesor wyliczający wartość minimalną i maksymalną.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MinMaxProcessor implements Processor {
    private static MinMaxProcessor instance = null;

    private MinMaxProcessor(){}

    /**
     * Zwraca procesor MinMaxProcessor.
     * @return instancja MinMaxProcessor
     */
    public static MinMaxProcessor getInstance() {
        if (instance == null) instance = new MinMaxProcessor();
        return instance;
    }

    /**
     * Wylicza wartość minimalną i maksymlną ze wszsytkich komórek modelu tabeli.
     * @param data model tabeli, na której komórkach wykonywana będzie operacja
     * @return min i max
     */
    @Override
    public String process(TableModel data) {
        double min = (Double) data.getValueAt(0, 0);
        double max = min;
        for (int i = 0; i < data.getRowCount(); i++) {
            for (int j = 0; j < data.getColumnCount(); j++) {
                double val = (Double) data.getValueAt(i, j);
                if (val < min) min = val;
                if (val > max) max = val;
            }
        }
        return "Min: " + min + "\tMax: " + max;
    }

    /**
     * Zwraca nazwe jako String.
     * @return "Min i Max"
     */
    @Override
    public String toString() {
        return "Min i Max";
    }
}
