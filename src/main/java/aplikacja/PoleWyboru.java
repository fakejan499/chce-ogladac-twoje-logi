package aplikacja;

import aplikacja.model.MyComboModel;

import javax.swing.*;

/**
 * Klasa panelu z wyborem operacji.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class PoleWyboru extends JPanel {
    private JComboBox comboBox;
    private MyFrame frame;

    /**
     * Domyślny konstruktor. Tworzy panel z polem combo z wyborem operacji (processora).
     *
     * @param frame główny frame aplikacji
     */
    PoleWyboru(MyComboModel model, MyFrame frame) {
        super();
        this.frame = frame;
        JLabel tfLabel = new JLabel("Operacja: ");
        add(tfLabel);

        comboBox = new JComboBox(model);
        comboBox.addActionListener((e -> {
            frame.onComboValChanged();
        }));
        add(comboBox);
    }
}
