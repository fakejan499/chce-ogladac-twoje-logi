package aplikacja.processor;

import javax.swing.table.TableModel;

/**
 * Interfejs procesora, zawierający jedną metodę process.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public interface Processor {
    /**
     * Jedyna metoda interfejsu.
     * @param data model tabeli, na której komórkach wykonywana będzie operacja
     * @return string z wynikiem operacji
     */
    String process(TableModel data);
}
