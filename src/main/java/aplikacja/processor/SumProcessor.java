package aplikacja.processor;

import javax.swing.table.TableModel;

/**
 * Procesor wyliczający sumę elementów.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class SumProcessor implements Processor {
    private static SumProcessor instance = null;

    private SumProcessor() {
    }

    /**
     * Zwraca procesor SumProcessor.
     * @return instancja SumProcessor
     */
    public static SumProcessor getInstance() {
        if (instance == null) instance = new SumProcessor();
        return instance;
    }

    /**
     * Wylicza sumę liczb ze wszsytkich komórek modelu tabeli.
     * @param data model tabeli, na której komórkach wykonywana będzie operacja
     * @return suma
     */
    @Override
    public String process(TableModel data) {
        double sum = 0;
        for (int i = 0; i < data.getRowCount(); i++) {
            for (int j = 0; j < data.getColumnCount(); j++) {
                sum += (Double) data.getValueAt(i, j);
            }
        }
        return String.valueOf(sum);
    }

    /**
     * Zwraca nazwe jako String.
     * @return "Suma"
     */
    @Override
    public String toString() {
        return "Suma";
    }
}
