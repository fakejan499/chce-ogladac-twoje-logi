package aplikacja;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

/**
 * Klasa panelu z tabelą na wartości.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyTable extends JPanel {
    private TableModel model;

    /**
     * Domyślny konstruktor tworzący panel z tabelą.
     *
     * @param model model dla tworzonej tabeli
     */
    public MyTable(TableModel model) {
        super();
        this.model = model;
        JTable table = new JTable(model);
        table.removeEditor();
        ((DefaultTableCellRenderer) table.getDefaultRenderer(Object.class)).setHorizontalAlignment(SwingConstants.RIGHT);
        add(table);
    }
}
