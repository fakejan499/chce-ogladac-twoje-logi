package aplikacja;

import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

/**
 * Klasa tool bara z kontrolkami używanymi przez aplikację.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyToolBar extends JToolBar {
    /**
     * Domyślny konstruktor. Tworzy ToolBar z kontorlkami resetowania, dodawania i zapisywania.
     * @param frame główny frame aplikacji
     */
    MyToolBar(MyFrame frame) {
        setLayout(new MigLayout("wrap 1"));
        JButton resetButton = new JButton("Resetuj");
        resetButton.setIcon(Icons.resizeImage(Icons.RESET, 20));
        resetButton.addActionListener(e -> frame.resetTable());
        add(resetButton, new CC().growX());

        JButton addButton = new JButton("Dodaj");
        addButton.setIcon(Icons.resizeImage(Icons.PLUS, 20));
        addButton.addActionListener(e -> frame.addValueToTable());
        add(addButton, new CC().growX());

        JButton saveButton = new JButton("Zapisz");
        saveButton.setIcon(Icons.resizeImage(Icons.SAVE, 20));
        saveButton.addActionListener(e -> frame.saveValuesToFile());
        add(saveButton, new CC().growX());
    }
}
