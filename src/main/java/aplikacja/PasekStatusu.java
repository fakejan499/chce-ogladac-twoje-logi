package aplikacja;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa panelu paska statusu.
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class PasekStatusu extends JPanel {
    private final Logger logger = LogManager.getLogger();
    private JLabel label;

    /**
     * Domyślny kontstruktor, tworzący panel z etykietą tekstową.
     * Wartość startowa etykiety to "Aplikacja działa"
     */
    public PasekStatusu() {
        super();
        label = new JLabel("Aplikacja działa");
        add(label);
        setBorder(BorderFactory.createMatteBorder(1, 0, 0,0, new Color(100, 100, 100)));
    }

    /**
     * Ustawia status (tekst w etykiecie tekstowej).
     * @param status nowy status
     */
    void setStatus(String status) {
        label.setText(status);
        logger.info(status);
    }
}
