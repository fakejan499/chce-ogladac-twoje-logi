package aplikacja;

/**
 * Klasa uruchomieniowa.
 * @author Dawid Szemro
 * @version 1.0.0 21/04/2021
 */
public class Main {
    public static void main(String[] args) {
        new MyFrame();
    }
}
