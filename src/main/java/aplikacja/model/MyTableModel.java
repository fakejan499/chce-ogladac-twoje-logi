package aplikacja.model;

import javax.swing.table.AbstractTableModel;

/**
 * Mój model dla komponentu table.
 *
 * @author Dawid Szemro
 * @version 1.0.0 23/04/2021
 */
public class MyTableModel extends AbstractTableModel {
    private int rowCount, columnCount;
    private Double[][] dataGrid;

    /**
     * Tworzy model z podaną liczbą kolumn i wierszy.
     *
     * @param rowCount    Liczba wierszy.
     * @param columnCount Liczba kolumn.
     */
    public MyTableModel(int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        dataGrid = new Double[rowCount][columnCount];
        resetTable();
    }

    /**
     * Pobiera ilość wierszy w modelu tabeli.
     *
     * @return ilość wierszy w modeu tabeli
     */
    @Override
    public int getRowCount() {
        return rowCount;
    }

    /**
     * Pobiera ilość kolumn w modelu tabeli.
     *
     * @return ilość kolumn w modeu tabeli
     */
    @Override
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * Sprawdza czy komórka jest edytowalna.
     *
     * @param row    Wiersz komórki.
     * @param column Kolumna komórki.
     * @return Zawsze false.
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    /**
     * Pobiera wartość komórki o podanych koordynatach.
     *
     * @param rowIndex    indeks wiesza
     * @param columnIndex indeks kolumny
     * @return wartość
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return dataGrid[rowIndex][columnIndex];
    }

    /**
     * Ustawia wartość w podanej komórce.
     *
     * @param aValue      nowa wartość dla komórki
     * @param rowIndex    indeks wiersza komórki
     * @param columnIndex indeks kolumny komórki
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        dataGrid[rowIndex][columnIndex] = (Double) aValue;
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    /**
     * Przywraca wszystkie wartości (wsyzstkie komórki) w modelu na 0.
     */
    public void resetTable() {
        for (int i = 0; i < getRowCount(); i++) {
            for (int j = 0; j < getColumnCount(); j++) {
                setValueAt(0d, i, j);
            }
        }
    }
}